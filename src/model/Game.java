package model;

import view.VPlaque;

import java.util.ArrayList;
import java.util.Stack;

public class Game {
	private int but, proposition;
	private String player;
	private ArrayList<Integer> listePlaques;
	private Stack<ArrayList<VPlaque>> memento;
	public static final int[] valuePool = new int [] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
											1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
											25, 50, 75, 100 
										  };

	public Game() {
		memento = new Stack<>();
		listePlaques = new ArrayList<>();
		initPlaques();
		initBut();
	}

	public void initPlaques() {
		listePlaques.clear();
		while (listePlaques.size() < 6) {
			int randomIndex = (int) (Math.random() * 23);
			if (valuePool[randomIndex] != 0) // Verifie si la plaque est disponible
			{
				listePlaques.add(valuePool[randomIndex]);
				valuePool[randomIndex] = 0; // Permet de rendre la plaque indisponible
			}
		}
	}

	public void setListePlaques(ArrayList<Integer> newListe) {
		this.listePlaques = new ArrayList<Integer>(newListe);
	}

	public ArrayList<Integer> getListePlaques() {
		return this.listePlaques;
	}

	public void addToListePlaques(Integer valeur) {
		listePlaques.add(valeur);
	}

	public void removeFromListePlaques(Integer valeur) {
		listePlaques.remove(valeur);
	}
	
	public Stack<ArrayList<VPlaque>> getMemento()
	{
		return this.memento;
	}
	
	// MEMENTO HERE
	public void flushMemento()
	{
		memento.clear();
	}
	
	public void pushMemento(ArrayList<VPlaque> currentList) {
		memento.push((ArrayList<VPlaque>)currentList.clone());
		System.out.println("Memento size: " + memento.size() + " new list size: " + currentList.size());
	}

	public void popMemento() {
		try {
			memento.pop();
			System.out.println("Memento size: " + memento.size());
		} catch (NullPointerException npe) {
			System.out.println("Memento vide !");
		}
	}

	public ArrayList<VPlaque> peekMemento() {
		try {
			ArrayList<VPlaque> vplist = (ArrayList<VPlaque>) memento.peek().clone();
			System.out.println("peek list size: " + vplist.size());
			return vplist;
		} catch (NullPointerException npe) {
			System.out.println("Memento vide !");
		}
		return null;
	}

	public void initBut() {
		this.but = (int) (Math.random() * (999 - 100)) + 100;
	}

	public int getBut() {
		return this.but;
	}

	public void setProposition(int proposition) {
		this.proposition = proposition;
	}

	public int getProposition() {
		return this.proposition;
	}

	public void setPlayer(String joueur) {
		this.player = joueur;
	}

	public String getPlayer() {
		return this.player;
	}
}
