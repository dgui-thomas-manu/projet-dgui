package controller;

public class GameControllerException extends Exception {
	private String message = "";

	public GameControllerException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
