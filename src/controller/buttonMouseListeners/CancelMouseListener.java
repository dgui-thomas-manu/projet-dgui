package controller.buttonMouseListeners;

import controller.SoundPlayer;
import view.VBoutonCancel;
import view.VZoneCalcul;
import view.VZoneCalcul.Position;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CancelMouseListener implements MouseListener {
	private VZoneCalcul zoneDeCalculActive;
	private VBoutonCancel owner;

	/*
	 * Besoin -> * Liste des plaques pour remettre les plaques a leur place * La
	 * zone de calcul active pour recuperer les plaques utilisees
	 */
	public CancelMouseListener(VBoutonCancel owner) {
		this.owner = owner;
	}

	@Override
	public void mouseClicked(MouseEvent me) {
	}

	@Override
	public void mouseEntered(MouseEvent me) {
	}

	@Override
	public void mouseExited(MouseEvent me) {
	}

	@Override
	public void mousePressed(MouseEvent me) {
		SoundPlayer.getInstance().playSound(SoundPlayer.SOUND.CANCEL);
		if (!owner.isEnabled())
			return;
		System.out.println("Clicked the cancel button.");

		zoneDeCalculActive = owner.getParentGui().getZoneDeCalculActive();
		if (zoneDeCalculActive == null)
			return;
		
		owner.getParentGui().activatePreviousZoneCalcul();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
}
