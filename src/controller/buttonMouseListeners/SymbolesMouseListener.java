package controller.buttonMouseListeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import view.VOperator;
import view.VZoneCalcul;

public class SymbolesMouseListener implements MouseListener {
	private VOperator source;
	private VZoneCalcul zoneDeCalculActive;

	public SymbolesMouseListener(VOperator source) {
		this.source = source;
		this.zoneDeCalculActive = source.getParentGui().getZoneDeCalculActive();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (!source.isEnabled())
			return;
		this.zoneDeCalculActive = source.getParentGui().getZoneDeCalculActive();
		if (zoneDeCalculActive == null)
			return;
		System.out.println("Symbol " + source.getText() + " was clicked.");

		zoneDeCalculActive.setSymbole(source.getText());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

}
