package controller.buttonMouseListeners;

import view.VPlaque;
import view.VZoneCalcul;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PlaqueMouseListener implements MouseListener {
	private VPlaque source;
	private VZoneCalcul zoneDeCalculActive;

	public PlaqueMouseListener(VPlaque source) {
		this.source = source;
		this.zoneDeCalculActive = source.getParentGui().getZoneDeCalculActive();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (!source.isEnabled())
			return;

		this.zoneDeCalculActive = source.getParentGui().getZoneDeCalculActive();
		if (zoneDeCalculActive == null) {
			System.out.println("Zone de calcul est a NULL!!");
			return;
		}

		System.out.println("Plaque " + source.getText() + " was clicked on.");
		source.setEnabled(false);
		if (!zoneDeCalculActive.setOperande(source.getText(),
				zoneDeCalculActive.getFirstFreeOperande()))
			source.setEnabled(true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

}
