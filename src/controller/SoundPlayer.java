package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class SoundPlayer
{
	public enum SOUND {
		GAMEOPEN, CANCEL, ERROR, ENDGAMEFAIL, PROPOSITIONLOCKIN, SELECT, ENDGAMEVICTORY,
	}

	private HashMap<SOUND, Audio> loadedSounds;
	private static SoundPlayer instance = new SoundPlayer();
	
	private SoundPlayer() {
		loadedSounds = new HashMap<>();
		init();
	}

	public static SoundPlayer getInstance() {
		if (instance == null)
			instance = new SoundPlayer();
		return instance;
	}

	private void init() {
		loadedSounds.put(SOUND.CANCEL, loadSound("cancel.wav", "WAV"));
		loadedSounds.put(SOUND.ENDGAMEFAIL, loadSound("fail.wav", "WAV"));
		loadedSounds.put(SOUND.ENDGAMEVICTORY, loadSound("victory.wav", "WAV"));
		loadedSounds.put(SOUND.ERROR, loadSound("error.wav", "WAV"));
		loadedSounds.put(SOUND.PROPOSITIONLOCKIN, loadSound("lockin.wav", "WAV"));
		loadedSounds.put(SOUND.SELECT, loadSound("select.wav", "WAV"));
	}
	
	public Audio loadSound(String fileName, String fileFormat)
	{
		Audio output = null;
		String basePath = "src/sounds/";
		String extension = fileFormat.toUpperCase();
		
		if(extension != null)
		{
			extension = extension.toUpperCase();
			try
			{
				output = AudioLoader.getAudio(extension, ResourceLoader.getResourceAsStream(basePath + fileName));
			} catch (IOException e)
			{
				System.out.println("N'a pas pu charger le son ayant comme chemin : " + fileName);
				output = null;
				return output;
			}
		}
		else
		{
			System.out.println("N'a pas pu charger le son ayant comme chemin : " + fileName);
		}
		
		return output;
	}

	public void playSound(SOUND which) {
		playSound(which, 1.0f, 1.0f, false);
	}

	public void playSound(SOUND which, boolean looped)
	{
		playSound(which, 1.0f, 1.0f, looped);
	}
	
	public void playSound(SOUND which, float pitch)
	{
		playSound(which, pitch, 1.0f, false);
	}

	public void playSound(SOUND which, float pitch, float gain)
	{
		playSound(which, pitch, gain, false);
	}
	
	public void playSound(SOUND which, float pitch, float gain, boolean looped)
	{
		if (!loadedSounds.containsKey(which)) {
			System.out.println("Le son ayant la cl� " + which.toString()
					+ " n'a pas �t� charg�.");
			return;
		}

		loadedSounds.get(which).playAsSoundEffect(pitch, gain, looped);
	}
}
