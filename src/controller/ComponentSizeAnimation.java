package controller;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ComponentSizeAnimation implements ActionListener {
	private enum DIRECTION {
		GROW, SHRINK, NONE,
	}

	public enum ANIMATIONTYPE {
		NORMAL, SHRINKNGROW,
	}

	// Le Timer qui fera office de "delayer"
	private Timer animationTimer;
	@SuppressWarnings("unused")
	private int animationDuration; // Duree totale de l'animation
	private int animationTickDuration; // Duree d'UNE ETAPE de l'animation

	// Le composant swing a� animer
	private JComponent componentToAnimate;
	private String initialText;

	// La dimension a�atteindre en fin d'animation
	private ANIMATIONTYPE animType;
	private Dimension targetDimension;
	private Point startingPosition; // pour ShrinkNGrow

	// Variables d'iterations
	private double iterationGrowRate;
	private boolean isPlaying = false;

	// Variables de direction de l'animation
	private DIRECTION WIDTH_DIRECTION;
	private DIRECTION HEIGHT_DIRECTION;

	/**
	 * Controleur s'occupant de l'animation de la taille d'un bouton
	 * (aggrandissement et retrecissement). Ce controleur doit absolument etre
	 * attache a� un objet de type Timer avant de pouvoir fonctionner.
	 * 
	 * @param target
	 *            L'objet JButton a faire grandir/retrecir.
	 * @param animType
	 *            Le type d'animation a� appliquer.
	 * @param targetSize
	 *            La dimension finale que l'objet doit avoir.
	 * @param animationDuration
	 *            La duree totale que l'animation prendra (en millisecondes!).
	 */
	public ComponentSizeAnimation(JComponent target, ANIMATIONTYPE animType,
			Dimension targetSize, int animationDuration)
			throws IllegalArgumentException {
		if (target == null)
			throw new IllegalArgumentException(
					"Le bouton a� manipuler ne peut etre NULL!");
		if (targetSize.getWidth() <= 0)
			throw new IllegalArgumentException(
					"La largeur cible doit etre > que 0.");
		if (targetSize.getHeight() <= 0)
			throw new IllegalArgumentException(
					"La hauteur cible doit etre > que 0.");

		this.componentToAnimate = target;
		this.targetDimension = targetSize;
		this.animType = animType;
		this.iterationGrowRate = 5;
		this.animationDuration = animationDuration;
		this.animationTickDuration = (int) (animationDuration / (100 / iterationGrowRate));

		if (target instanceof JLabel)
			initialText = ((JLabel) target).getText();
		if (target instanceof JButton)
			initialText = ((JButton) target).getText();

		animationTimer = new Timer(animationTickDuration, this);
		animationTimer.setRepeats(true);
	}

	/**
	 * Methode a� appeler pour demarrer l'animation pre-initialisee.
	 */
	public synchronized void startAnimation() {
		if (Globals.LOGGING_ENABLED)
			System.out.println("Starting resize of component: "
					+ componentToAnimate.getClass().getCanonicalName()
					+ " FROM " + componentToAnimate.getWidth() + ", "
					+ componentToAnimate.getHeight() + " TO "
					+ targetDimension.getWidth() + ", "
					+ targetDimension.getHeight());

		if (animType == ANIMATIONTYPE.SHRINKNGROW) {
			WIDTH_DIRECTION = DIRECTION.SHRINK;
			HEIGHT_DIRECTION = DIRECTION.NONE;
			this.targetDimension = componentToAnimate.getSize();
			this.startingPosition = componentToAnimate.getLocation();
		} else if (animType == ANIMATIONTYPE.NORMAL) {
			if (componentToAnimate.getWidth() < targetDimension.getWidth())
				WIDTH_DIRECTION = DIRECTION.GROW;
			else
				WIDTH_DIRECTION = DIRECTION.SHRINK;

			if (componentToAnimate.getHeight() < targetDimension.getHeight())
				HEIGHT_DIRECTION = DIRECTION.GROW;
			else
				HEIGHT_DIRECTION = DIRECTION.SHRINK;
		}

		isPlaying = true;

		if (componentToAnimate instanceof JLabel)
			((JLabel) componentToAnimate).setText("");
		if (componentToAnimate instanceof JButton)
			((JButton) componentToAnimate).setText("");

		animationTimer.start();
	}

	public void stopAnimation() {
		if (Globals.LOGGING_ENABLED)
			System.out.println("End of animation, final size == "
					+ componentToAnimate.getWidth() + ", " + ""
					+ componentToAnimate.getHeight());

		if (animType == ANIMATIONTYPE.SHRINKNGROW)
			WIDTH_DIRECTION = DIRECTION.SHRINK;

		animationTimer.stop();

		if (componentToAnimate instanceof JLabel)
			((JLabel) componentToAnimate).setText(initialText);
		if (componentToAnimate instanceof JButton)
			((JButton) componentToAnimate).setText(initialText);

		isPlaying = false;
		componentToAnimate.setLocation(startingPosition);
	}

	public synchronized boolean isAnimationPlaying() {
		return isPlaying;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		double newWidth = componentToAnimate.getSize().width, newHeight = componentToAnimate
				.getSize().height;

		// On prend note de la taille actuelle du bouton.
		Dimension currentSize = componentToAnimate.getSize();

		// On calcule la nouvelle hauteur/largeur du bouton en ajoutant (ou
		// retirant) X % a� sa taille actuelle.
		switch (WIDTH_DIRECTION) {
		case GROW:
			newWidth = ((currentSize.width) + (currentSize.width
					* (iterationGrowRate / 100) + iterationGrowRate));
			if (animType == ANIMATIONTYPE.SHRINKNGROW) {
				Point newPosition = new Point(
						(int) (componentToAnimate.getLocation().x - iterationGrowRate),
						componentToAnimate.getLocation().y);
				componentToAnimate.setLocation(newPosition);
			}
			break;

		case SHRINK:
			newWidth = ((currentSize.width) - (currentSize.width
					* (iterationGrowRate / 100) + iterationGrowRate));
			if (animType == ANIMATIONTYPE.SHRINKNGROW) {
				Point newPosition = new Point(
						(int) (componentToAnimate.getLocation().x + iterationGrowRate),
						componentToAnimate.getLocation().y);
				componentToAnimate.setLocation(newPosition);
			}
			break;
		case NONE:
			break;
		}

		switch (HEIGHT_DIRECTION) {
		case GROW:
			newHeight = (currentSize.height + (currentSize.height
					* (iterationGrowRate / 100) + 1));
			break;

		case SHRINK:
			newHeight = (currentSize.height - (currentSize.height
					* (iterationGrowRate / 100) + 1));
			break;
		case NONE:
			break;
		}

		// Controle de la taille pour qu'elle ne depasse pas la taille
		// finale a� atteindre.
		if (animType == ANIMATIONTYPE.NORMAL) {
			if (WIDTH_DIRECTION == DIRECTION.GROW)
				if (newWidth > (int) targetDimension.getWidth())
					newWidth = (int) targetDimension.getWidth();
			if (WIDTH_DIRECTION == DIRECTION.SHRINK)
				if (newWidth < (int) targetDimension.getWidth())
					newWidth = (int) targetDimension.getWidth();

			if (HEIGHT_DIRECTION == DIRECTION.GROW)
				if (newHeight > (int) targetDimension.getHeight())
					newHeight = (int) targetDimension.getHeight();
			if (HEIGHT_DIRECTION == DIRECTION.SHRINK)
				if (newHeight < (int) targetDimension.getHeight())
					newHeight = (int) targetDimension.getHeight();
		} else if (animType == ANIMATIONTYPE.SHRINKNGROW) {
			switch (WIDTH_DIRECTION) {
			case SHRINK:
				if (newWidth <= 0) {
					newWidth = 0;
					this.WIDTH_DIRECTION = DIRECTION.GROW;
				}
				break;

			case GROW:
				if (newWidth > (int) targetDimension.getWidth())
					newWidth = (int) targetDimension.getWidth();
				break;

			case NONE:
				break;

			default:
				break;
			}
		}

		// Application de la nouvelle taille temporaire
		componentToAnimate.setSize(new Dimension((int) newWidth,
				(int) newHeight));

		// VÃ©rifier si on peut arreter le Timer.
		if (componentToAnimate.getWidth() == targetDimension.getWidth()
				&& componentToAnimate.getHeight() == targetDimension
						.getHeight())
			stopAnimation();
	}
}
