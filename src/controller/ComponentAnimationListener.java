package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ComponentAnimationListener implements ActionListener {
	private ComponentSizeAnimation[] queuedAnimations;
	private ComponentSizeAnimation currentlyPlaying = null;
	private int nextAnimationID = 0;
	private int animationQueueSize;
	private boolean isReady;

	/**
	 * Listener captant l'evenement ActionPerformed sur le JComponent a  animer.
	 * Par deaut, l'animation n'est pas repetable.
	 * 
	 * @param animations
	 *            Un Array d'animations qui seront mises en file d'attente.
	 */
	public ComponentAnimationListener(ComponentSizeAnimation[] animations) {
		queuedAnimations = animations;
		animationQueueSize = animations.length;
		isReady = true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (isReady) {
			// System.out.println("Im ready");
			isReady = false;

			currentlyPlaying = queuedAnimations[nextAnimationID];
			currentlyPlaying.startAnimation();

			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					while (!isReady) {
						// System.out.println("Im playing");
						if (!currentlyPlaying.isAnimationPlaying()) {
							++nextAnimationID;
							if (nextAnimationID >= animationQueueSize) {
								isReady = true;
								nextAnimationID = 0;
								return;
							} else {
								currentlyPlaying = queuedAnimations[nextAnimationID];
								currentlyPlaying.startAnimation();
							}
						}
					}
				}
			});
			t.start();
		}
	}
}
