package controller;

import model.Game;
import view.VOperande;
import view.VPlaque;
import view.VSymbole;

import java.util.ArrayList;

public class GameController {
	private final Game game;

	public GameController() {
		game = new Game();
	}

	public void initPlaques() {
		game.initPlaques();
	}

	public void initBut() {
		game.initBut();
	}

	public ArrayList<Integer> getListePlaques() {
		return game.getListePlaques();
	}

	public void setListePlaques(ArrayList<Integer> newListe) {
		game.setListePlaques(newListe);
	}

	public void addToListePlaques(Integer valeur) {
		game.addToListePlaques(valeur);
	}

	public void removeFromListePlaques(Integer valeur) {
		game.removeFromListePlaques(valeur);
	}
	
	public void flushMemento()
	{
		game.flushMemento();
	}

	public void pushMemento(ArrayList<VPlaque> currentList) {
		game.pushMemento(currentList);
	}

	public void popMemento() {
		try {
			game.popMemento();
		} catch (NullPointerException npe) {
			System.out.println("Memento vide !");
		}
	}

	public ArrayList<VPlaque> peekMemento() {
		try {
			return game.peekMemento();
		} catch (NullPointerException npe) {
			System.out.println("Memento vide !");
		}
		return null;
	}

	public int getBut() {
		return game.getBut();
	}

	public boolean checkProposition(String proposition) {
		try {
			int prop = Integer.parseInt(proposition);
			if (prop < 90 || prop > 1009)
				return false;
			else
				return true;
		} catch (IllegalArgumentException iae) {
			return false;
		}
	}

	public int getProposition() {
		return game.getProposition();
	}

	public String getPlayer() {
		return game.getPlayer();
	}

	public int checkOperation(VOperande operande1, VSymbole symbole,
			VOperande operande2) throws GameControllerException {
		int op1 = operande1.getValeur();
		int op2 = operande2.getValeur();
		char op = symbole.getValeur().charAt(0);
		int resultat = -1;

		switch (op) {
		case '+':
			resultat = op1 + op2;
			break;

		case 'x':
			resultat = op1 * op2;
			break;

		case '-':
			if (op1 - op2 < 0)
				throw new GameControllerException(
						"Erreur : Le resultat de la soustraction "
								+ "est negatif.  Operation non autorisee.");
			else
				resultat = op1 - op2;
			break;

		case '/':
			if (op1 % op2 != 0)
				throw new GameControllerException(
						"Erreur : Le resultat de la division n'est "
								+ "pas entier.  Operation non autorisee.");
			else
				resultat = op1 / op2;
			break;

		default:
			resultat = -1;
			throw new GameControllerException(
					"Une erreur est survenue lors du calcul.");
		}

		return resultat;
	}

	public boolean intermediateEndgameCheck(VOperande resultat,
			String propositionStr) {
		int proposition = Integer.parseInt(propositionStr);
		int result = resultat.getValeur();

		return (proposition == result) ? true : false;
	}

	public int calcPoints(VOperande resultat, String propositionStr) {
		int proposition = Integer.parseInt(propositionStr);
		int result = resultat.getValeur();
		int points = -1;

		if (result == proposition) {
			SoundPlayer.getInstance().playSound(SoundPlayer.SOUND.ENDGAMEVICTORY);
			if (proposition < game.getBut())
				points = 10 - (game.getBut() - proposition);
			else
				points = 10 - (proposition - game.getBut());
			if (points < 0)
				points = 0;
		} else {
			points = 0;
			SoundPlayer.getInstance().playSound(SoundPlayer.SOUND.ENDGAMEFAIL);
		}

		return points;
	}
}
