package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

public class VOperande extends JLabel {
	/**
     * 
     */
	private static final long serialVersionUID = -6530307350180966666L;

	private boolean isInUse = false;

	private VZoneCalcul parent;

	public VOperande(VZoneCalcul parent) {
		this.parent = parent;

		this.setPreferredSize(new Dimension(85, 60));
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		this.setBackground(Color.WHITE);
		this.setOpaque(true);
		this.setFont(new Font("sansserif", Font.PLAIN, 35));
		this.setHorizontalAlignment(JLabel.CENTER);
		this.setVerticalAlignment(JLabel.CENTER);
		this.setVisible(true);
	}

	public boolean isInuse() {
		return isInUse;
	}

	public int getValeur() {
		return Integer.parseInt(this.getText());
	}

	@Override
	public void setText(String text) {
		super.setText(text);
		if (text.equals(""))
			isInUse = false;
		else
			isInUse = true;
	}

	public VZoneCalcul getZoneCalculParent() {
		return parent;
	}
}
