package view.view.subWindows;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created with IntelliJ IDEA.
 * User: Oscuro
 * Date: 29/12/13
 * Time: 17:48
 * To change this template use File | Settings | File Templates.
 */
public class Credits extends JFrame
{
	private JPanel mainPanel, textPanel, boutonsPanel;
	private JButton btn_okay; 
	
	private JLabel creditsText;
	private final String creditsString = "<html><i><b><u><center>Le compte est " +
			"bon!</center></u></b></i><br/><br/>Code par Thomas & Manu<br/><br/>v0.0.0.0.1 pre-alpha</html>";

	private ImageIcon logoIcon;
	private JLabel logo;
	
	public Credits()
	{
		setSize(400, 300);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Credits !");
		setUndecorated(true);
		setVisible(true);
		
		initPanels();
		initComponents();
		
		add(mainPanel);
		
		initListeners();
		
		setLocationRelativeTo(null);
	}

	private void initPanels()
	{
		mainPanel = new JPanel(new BorderLayout());
		
		textPanel = new JPanel(new GridLayout(2,1));
		boutonsPanel = new JPanel(new FlowLayout());
		
		mainPanel.add(textPanel, BorderLayout.NORTH);
		mainPanel.add(boutonsPanel, BorderLayout.SOUTH);
	}

	private void initComponents()
	{
		// Bouton OKAY
		btn_okay = new JButton("Okay !");
		btn_okay.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				dispose();
			}
		});
		boutonsPanel.add(btn_okay);
		
		// Labels
		creditsText = new JLabel(creditsString);
		creditsText.setHorizontalAlignment(SwingConstants.CENTER);
		creditsText.setHorizontalTextPosition(SwingConstants.CENTER);
		creditsText.setForeground(Color.DARK_GRAY);
		textPanel.add(creditsText,0);
		 
		int logoSelector = randomWithRange(1,3);
		logoIcon = new ImageIcon("src/pictures/credits" + logoSelector + ".gif");
		setSize(new Dimension(logoIcon.getIconWidth() + 10, getHeight()));
		logo = new JLabel(logoIcon);
		logo.setHorizontalTextPosition(SwingConstants.CENTER);
		logo.setHorizontalAlignment(SwingConstants.CENTER);
		textPanel.add(logo,1);
		
		mainPanel.setBackground(Color.BLACK);

		textPanel.setOpaque(false);
		boutonsPanel.setOpaque(false);
	}

	private int randomWithRange(int min, int max)
	{
		int range = (max - min) + 1;
		return (int)(Math.random() * range) + min;
	}
	
	private void initListeners()
	{
		this.addWindowListener(new CreditsWindowAdapter(this));
	}

	private class CreditsWindowAdapter extends WindowAdapter
	{
		private JFrame listenedWindow;
		
		public CreditsWindowAdapter(JFrame source)
		{
			listenedWindow = source;
		}
		
		@Override
		public void windowDeactivated(WindowEvent e)
		{
			listenedWindow.dispose();
		}
	}
}
