package view;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class VBoutonCancel extends JButton {
	/**
     * 
     */
	private static final long serialVersionUID = -4473217309000044013L;
	private VGame parentGui;

	public VBoutonCancel(VGame parent) {
		this.parentGui = parent;
		this.setBackground(Color.LIGHT_GRAY);
		this.setIcon(new ImageIcon("src/pictures/deleteIcon.png"));
		this.setPreferredSize(new Dimension(50, 50));
		this.setVisible(true);
	}

	public VGame getParentGui() {
		return parentGui;
	}
}
