package view;

import view.view.subWindows.Credits;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainFrame extends JFrame {
	JPanel mainPanel, logoPanel, buttonPanel;
	JLabel backgroundLogo, logoGame;
	JButton newGame, exit, credits;
	Dimension mainButtonDimension;
	Font mainButtonFont;

	MainFrameButtonListener mfbl;

	public MainFrame() {
		this.setSize(640, 480);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setUndecorated(true);
		this.setTitle("Le compte est bon !");
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		backgroundLogo = new JLabel(new ImageIcon("src\\pictures\\LCEB_Background.png"));
		backgroundLogo.setLayout(new FlowLayout());
		this.add(backgroundLogo);
		
		mfbl = new MainFrameButtonListener(this);

		mainPanel = new JPanel();
		mainPanel.setOpaque(false);
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		backgroundLogo.add(mainPanel);

		logoPanel = new JPanel();
		logoPanel.setOpaque(false);
		mainPanel.add(logoPanel);
		
		logoGame = new JLabel(new ImageIcon("src\\pictures\\LCEB_Banner.png"));
		logoPanel.add(logoGame);
		
		mainPanel.add(Box.createRigidArea(new Dimension(0, 100)));
		
		buttonPanel = new JPanel();
		buttonPanel.setOpaque(false);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		mainPanel.add(buttonPanel);

		mainButtonDimension = new Dimension(300, 50);
		mainButtonFont = new Font("sansserif", Font.PLAIN, 40);

		newGame = new JButton("Nouvelle partie");
		newGame.setPreferredSize(mainButtonDimension);
		newGame.setAlignmentX(Component.CENTER_ALIGNMENT);
		newGame.setBackground(Color.LIGHT_GRAY);
		newGame.setForeground(Color.WHITE);
		newGame.setFont(mainButtonFont);
		buttonPanel.add(newGame);
		buttonPanel.add(Box.createRigidArea(new Dimension(0,5)));

		credits = new JButton("Credits");
		credits.setPreferredSize(mainButtonDimension);
		credits.setAlignmentX(Component.CENTER_ALIGNMENT);
		credits.setFont(mainButtonFont);
		credits.setBackground(Color.LIGHT_GRAY);
		credits.setForeground(Color.WHITE);
		buttonPanel.add(credits);
		buttonPanel.add(Box.createRigidArea(new Dimension(0,5)));
		
		exit = new JButton("Quitter");
		exit.setPreferredSize(mainButtonDimension);
		exit.setAlignmentX(Component.CENTER_ALIGNMENT);
		exit.setFont(mainButtonFont);
		exit.setBackground(Color.LIGHT_GRAY);
		exit.setForeground(Color.WHITE);
		buttonPanel.add(exit);
		buttonPanel.add(Box.createRigidArea(new Dimension(0,5)));

		newGame.addActionListener(mfbl);
		exit.addActionListener(mfbl);
		credits.addActionListener(mfbl);
	}

	private class MainFrameButtonListener implements ActionListener {
		private MainFrame parent;

		public MainFrameButtonListener(MainFrame parent) {
			this.parent = parent;
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand().equals("Quitter"))
				System.exit(0);

			if (ae.getActionCommand().equals("Nouvelle partie")) {
				new VGame();
				parent.dispose();
			}

			if (ae.getActionCommand().equals("Credits")) {
				new Credits();
			}
		}
	}
}
