package view;

import controller.GameControllerException;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class VZoneCalcul extends JPanel {
	private VBoutonConfirm confirm;
	private VOperande op1, op2;
	private VSymbole symbole;
	private VSymbole egal;
	private VOperande resultat;
	private boolean wasCalculated;
	private VGame parentGui;

	public VZoneCalcul(VGame parent) {
		this.parentGui = parent;
		this.setOpaque(false);

		confirm = new VBoutonConfirm(this);
		op1 = new VOperande(this);
		op2 = new VOperande(this);
		symbole = new VSymbole(this);
		egal = new VSymbole(this, "=");
		egal.setFont(new Font("sansserif", Font.BOLD, 18));
		resultat = new VOperande(this);
		wasCalculated = false;
	}

	@Override
	public void setEnabled(boolean enabled) {
		op1.setEnabled(enabled);
		op2.setEnabled(enabled);
		symbole.setEnabled(enabled);
		egal.setEnabled(enabled);
		confirm.setEnabled(enabled);
	}

	public Position getFirstFreeOperande() {
		if (op1.getText().equals(""))
			return Position.FIRST;
		else if (op2.getText().equals(""))
			return Position.SECOND;
		return null;
	}

	public boolean setOperande(String valeur, Position which) {
		if (which == null)
			return false;

		switch (which) {
		case FIRST:
			if (op1.isInuse())
				return false;
			else
				op1.setText(valeur);
			return true;
		case SECOND:
			if (op2.isInuse())
				return false;
			else
				op2.setText(valeur);
			return true;
		}

		return false;
	}
	
	public boolean tryCalculResultat() {
		if (!op1.getText().equals("") && !op2.getText().equals("")
				&& !symbole.getText().equals("") && !wasCalculated) {
			System.out.println("Calcul du resultat via le controleur...");

			int resultat = 0;
			try {
				resultat = parentGui.getGameController().checkOperation(op1,
						symbole, op2);
				this.resultat.setText("" + resultat);
				wasCalculated = true;

				if (parentGui.getGameController().intermediateEndgameCheck(this.resultat, parentGui.getProposition()))
					parentGui.intermediateEndgameDetected();
				else if (!parentGui.isLastZoneCalcul() && !parentGui.getIntermediateEndgameDetected())
				{
					System.out.println("Passage a la zone de calcul suivante...");
					parentGui.activateNextZoneCalcul();
				}
				else
					parentGui.checkEndgameConditions();

				return true;
			} catch (GameControllerException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}

		return false;
	}

	public void clearZoneDeCalcul() {
		op1.setText("");
		op2.setText("");
		resultat.setText("");
		symbole.setText("");
		wasCalculated = false;
	}

	public VOperande getOperande(Position which) {
		switch (which) {
		case FIRST:
			return op1;
		case SECOND:
			return op2;
		default:
			return null;
		}
	}

	public VSymbole getSymbole() {
		return symbole;
	}

	public void setSymbole(String newSymbole) {
		symbole.setText(newSymbole);
	}

	public JLabel getEgal() {
		return egal;
	}

	public VOperande getResultField() {
		return resultat;
	}

	public VGame getParentGui() {
		return parentGui;
	}

	public VBoutonConfirm getBoutonConfirm() {
		return confirm;
	}

	public enum Position {
		FIRST, SECOND,
	}
	
	public boolean isEmptyLine()
	{
		return (op1.getText().equals("") && op2.getText().equals("") && symbole.getText().equals(""))?true:false;
	}
}
