package view;

import controller.SoundPlayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class VBoutonConfirm extends JButton {
	/**
     * 
     */
	private static final long serialVersionUID = 4620976564385661874L;
	private final VZoneCalcul parentZone;

	public VBoutonConfirm(VZoneCalcul parent) {
		this.parentZone = parent;
		this.setBackground(Color.LIGHT_GRAY);
		this.setIcon(new ImageIcon("src/pictures/confirmIcon.png"));
		this.setPreferredSize(new Dimension(65, 40));
		this.setVisible(true);
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
				SoundPlayer.getInstance().playSound(SoundPlayer.SOUND.SELECT);
				parentZone.tryCalculResultat();
			}
		});
	}

	public VZoneCalcul getParentGui() {
		return parentZone;
	}
}
