package view;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class VBut extends JPanel {
	/**
     * 
     */
	private static final long serialVersionUID = 8568363824978435430L;
	private String goal;
	private VGame parentGui;

	public VBut(VGame parent, int but) {
		this.parentGui = parent;

		this.goal = Integer.toString(parentGui.getGameController().getBut());
		Font fontVBut = new Font("sansserif", Font.BOLD, 30);

		Color mainColor = Color.DARK_GRAY;//new Color(220, 220, 220);
		Color fontColor = Color.WHITE;
		
		Border mainBorder = BorderFactory.createRaisedBevelBorder();
		Border insideBorder = BorderFactory.createLoweredBevelBorder();

		this.setBorder(mainBorder);
		this.setBackground(mainColor);
		this.setOpaque(true);

		JLabel chiffreC = new JLabel(Character.toString(goal.charAt(0)));
		chiffreC.setBorder(insideBorder);
		chiffreC.setBackground(mainColor);
		chiffreC.setForeground(fontColor);
		chiffreC.setOpaque(true);
		chiffreC.setFont(fontVBut);

		JLabel chiffreD = new JLabel(Character.toString(goal.charAt(1)));
		chiffreD.setBorder(insideBorder);
		chiffreD.setBackground(mainColor);
		chiffreD.setForeground(fontColor);
		chiffreD.setOpaque(true);
		chiffreD.setFont(fontVBut);

		JLabel chiffreU = new JLabel(Character.toString(goal.charAt(2)));
		chiffreU.setBorder(insideBorder);
		chiffreU.setBackground(mainColor);
		chiffreU.setForeground(fontColor);
		chiffreU.setOpaque(true);
		chiffreU.setFont(fontVBut);

		this.add(chiffreC);
		this.add(chiffreD);
		this.add(chiffreU);
	}

	public VGame getParentGui() {
		return parentGui;
	}
}