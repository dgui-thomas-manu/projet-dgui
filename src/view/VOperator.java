package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.*;

@SuppressWarnings("serial")
public class VOperator extends JButton {
	private String value = "";

	private VZoneCalcul parentZC;
	private VGame parentGui;

	public VOperator(VZoneCalcul parent) {
		this.parentZC = parent;
		parentGui = null;

		this.setPreferredSize(new Dimension(60, 60));
		this.setEnabled(true);
		this.setText(value);
		this.setHorizontalAlignment(CENTER);
		this.setVerticalAlignment(CENTER);
		this.setBackground(Color.LIGHT_GRAY);
		this.setForeground(Color.WHITE);
		this.setFont(new Font("sansserif", Font.BOLD, 25));
		this.setVisible(true);
	}

	public VOperator(VZoneCalcul parent, String valeur) {
		this(parent);

		this.value = valeur;
		this.setPreferredSize(new Dimension(60, 60));
		this.setBackground(Color.LIGHT_GRAY);
		this.setForeground(Color.WHITE);
		this.setFont(new Font("sansserif", Font.BOLD, 25));
		this.setEnabled(true);
		this.setText(value);
		this.setVisible(true);
	}

	public VOperator(VGame parent, String valeur) {
		parentGui = parent;
		parentZC = null;

		this.setPreferredSize(new Dimension(60, 60));
		this.setEnabled(true);
		this.setText(value);
		this.setHorizontalAlignment(CENTER);
		this.setVerticalAlignment(CENTER);
		this.setBackground(Color.LIGHT_GRAY);
		this.setForeground(Color.WHITE);
		this.setFont(new Font("sansserif", Font.BOLD, 25));
		this.setVisible(true);
		this.setEnabled(true);
		this.value = valeur;
		this.setText(value);
	}

	public void setValeur(String valeur) {
		this.value = valeur;
		this.setText(value);
	}

	public String getValeur() {
		return this.getText();
	}

	public VZoneCalcul getZoneCalculParent() {
		return parentZC;
	}

	public VGame getParentGui() {
		return parentGui;
	}
}
