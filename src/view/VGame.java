package view;

import controller.GameController;
import controller.SoundPlayer;
import controller.VGameWinListener;
import controller.buttonMouseListeners.CancelMouseListener;
import controller.buttonMouseListeners.PlaqueMouseListener;
import controller.buttonMouseListeners.SymbolesMouseListener;
import view.VZoneCalcul.Position;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class VGame extends JFrame {
	/**
	 *
	 */
	private static final long serialVersionUID = 406572671212311471L;
	private GameController gControl = new GameController();
	private ArrayList<VPlaque> plaquesTab;
	private List<VZoneCalcul> zonesDeCalcul;
	private VZoneCalcul zoneDeCalculActive = null;
	private JPanel logoPanel, framePanel, symbolesPanel, butPropositionPanel, butPanel, propositionPanel,
			plaquesPanel, zoneCalculEtSymbolesPanel;
	private JLabel logoGame, backgroundLogo, butLabel, propositionLabel;
	private VBoutonCancel annulerDerniereOperation;
	private VBut but;

	final Color backgroundColor = new Color(91, 91, 91);
	final JButton entrerProp = new JButton("Ok");
	
	private final JTextArea propositionTA;
	private VOperator plus, moins, mult, div;
	private boolean isLastZoneCalcul, isEndGameDetected;

	public VGame() {
		plaquesTab = new ArrayList<>();
		zonesDeCalcul = new ArrayList<>();

		isLastZoneCalcul = false;
		isEndGameDetected = false;
		
		this.setLayout(new BorderLayout());
		//this.setUndecorated(true);
		this.setSize(new Dimension(800, 600));
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setTitle("Le compte est bon !");
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		
		// Permet d'utiliser une image en fond de fenetre
		backgroundLogo = new JLabel(new ImageIcon("src\\pictures\\LCEB_Background.png"));
		backgroundLogo.setLayout(new FlowLayout());
		this.add(backgroundLogo);

		// Construction de la disposition de la fenetre
		// Panel principal
		framePanel = new JPanel();
		framePanel.setOpaque(false);
		framePanel.setLayout(new BoxLayout(framePanel, BoxLayout.PAGE_AXIS));
		backgroundLogo.add(framePanel);

		logoPanel = new JPanel();
		logoPanel.setOpaque(false);
		framePanel.add(logoPanel);
		
		logoGame = new JLabel(new ImageIcon("src\\pictures\\LCEB_Banner.png"));
		logoPanel.add(logoGame);
		
		// Panel reserve a l'affichage du but et de la proposition
		
		Font butPropFont = new Font("sansserif", Font.BOLD, 30); //Police g�n�rale pour but et proposition

		butPropositionPanel = new JPanel();
		//butPropositionPanel.setOpaque(false);
		butPropositionPanel.setBackground(backgroundColor);
		butPropositionPanel.setBorder(BorderFactory.createRaisedBevelBorder());
		framePanel.add(butPropositionPanel);
		
		butPanel = new JPanel();
		//butPanel.setBackground(backgroundColor);
		butPanel.setOpaque(false);
		butPropositionPanel.add(butPanel);
		butPropositionPanel.add(Box.createRigidArea(new Dimension(150, 0)));
		propositionPanel = new JPanel();
		//propositionPanel.setBackground(backgroundColor);
		propositionPanel.setOpaque(false);
		butPropositionPanel.add(propositionPanel);
		
		butLabel = new JLabel("But :");
		butLabel.setForeground(Color.WHITE);
		butLabel.setFont(butPropFont);
		butPanel.add(butLabel);
		but = new VBut(this, gControl.getBut());
		butPanel.add(but);


		propositionLabel = new JLabel("Proposition :");
		propositionLabel.setForeground(Color.WHITE);
		propositionLabel.setFont(butPropFont);
		propositionPanel.add(propositionLabel);
		
		propositionTA = new JTextArea();
		propositionTA.setPreferredSize(new Dimension(55, 40));
		propositionTA.setBorder(BorderFactory.createLoweredBevelBorder());
		propositionTA.setBackground(Color.DARK_GRAY);
		propositionTA.setForeground(Color.WHITE);
		propositionTA.setFont(butPropFont);
		propositionPanel.add(propositionTA);
		
		this.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowOpened(WindowEvent e)
			{
				super.windowOpened(e);
				propositionTA.requestFocus();
			}
		});

		entrerProp.setBackground(Color.LIGHT_GRAY);
		entrerProp.setForeground(Color.WHITE);
		propositionPanel.add(entrerProp);
		
		entrerProp.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				if (!entrerProp.isEnabled())
					return;
				if (!gControl.checkProposition(propositionTA.getText())) {
					JOptionPane.showMessageDialog(null,
							"Veuillez n'entrer qu'un entier entre 90 et 1009!");
					return;
				}
				SoundPlayer.getInstance().playSound(
						SoundPlayer.SOUND.PROPOSITIONLOCKIN);
				activateNextZoneCalcul();
				propositionTA.setEnabled(false);
				entrerProp.setEnabled(false);
				System.out.println("Proposition locked in");
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		propositionTA.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyPressed(KeyEvent ke) {
				if (!entrerProp.isEnabled())
					return;
				if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
					if (!gControl.checkProposition(propositionTA.getText())) {
						JOptionPane
								.showMessageDialog(null,
										"Veuillez n'entrer qu'un entier entre 90 et 1009!");
						return;
					}
					SoundPlayer.getInstance().playSound(
							SoundPlayer.SOUND.PROPOSITIONLOCKIN);
					activateNextZoneCalcul();
					propositionTA.setEnabled(false);
					entrerProp.setEnabled(false);
					System.out.println("Proposition locked in");
				}
			}
		});

		// Panel rÃ©servÃ© Ã  l'affichage des plaques
		plaquesPanel = new JPanel();
		plaquesPanel.setOpaque(false);
		framePanel.add(plaquesPanel);

		addPlaquesDeDepart(gControl.getListePlaques());

		zoneCalculEtSymbolesPanel = new JPanel(new FlowLayout());
		zoneCalculEtSymbolesPanel.setOpaque(false);


		setupZonesDeCalcul(zoneCalculEtSymbolesPanel);

		plus = new VOperator(this, "+");
		plus.addMouseListener(new SymbolesMouseListener(plus));
		moins = new VOperator(this, "-");
		moins.addMouseListener(new SymbolesMouseListener(moins));
		mult = new VOperator(this, "x");
		mult.addMouseListener(new SymbolesMouseListener(mult));
		div = new VOperator(this, "/");
		div.addMouseListener(new SymbolesMouseListener(div));

		symbolesPanel = new JPanel(new GridLayout(4, 0));
		symbolesPanel.setOpaque(false);
		symbolesPanel.add(plus, 0);
		symbolesPanel.add(moins, 1);
		symbolesPanel.add(mult, 2);
		symbolesPanel.add(div, 3);

		zoneCalculEtSymbolesPanel.add(symbolesPanel, BorderLayout.EAST);
		framePanel.add(zoneCalculEtSymbolesPanel);

		annulerDerniereOperation = new VBoutonCancel(this);
		zoneCalculEtSymbolesPanel.add(annulerDerniereOperation);
		annulerDerniereOperation.addMouseListener(new CancelMouseListener(
				annulerDerniereOperation));
		
		this.addWindowListener(new VGameWinListener(this));
	}

	public void activatePreviousZoneCalcul() {
		if (zoneDeCalculActive == null)// || zoneDeCalculActive == zonesDeCalcul.get(zonesDeCalcul.size()-1))
			return;
		else {
			int currentZCIndex = zonesDeCalcul.indexOf(zoneDeCalculActive);
			int targetIndex = currentZCIndex + 1;

			boolean wasEmptyLine = zoneDeCalculActive.isEmptyLine();
			retablirPlaques();
			zoneDeCalculActive.clearZoneDeCalcul();
			
			if (targetIndex >= zonesDeCalcul.size() || targetIndex < 0)
				return; 
			
			if(wasEmptyLine)
			{
				zoneDeCalculActive.setEnabled(false);
				restoreMemento();
			
				zoneDeCalculActive = zonesDeCalcul.get(targetIndex);
			
				retablirPlaques();
				zoneDeCalculActive.clearZoneDeCalcul();
				zoneDeCalculActive.setEnabled(true);
			}
			
			if (isLastZoneCalcul)
				isLastZoneCalcul = false;
		}
	}

	public void activateNextZoneCalcul() {
		if (isLastZoneCalcul || isEndGameDetected)
			return;
		if (zoneDeCalculActive == null) {
			zoneDeCalculActive = zonesDeCalcul.get(zonesDeCalcul.size() - 1);
			zoneDeCalculActive.setEnabled(true);
		} else {
			zoneDeCalculActive.setEnabled(false);
			
			pushMemento();
			
			addPlaqueFromResultat(Integer.parseInt(zoneDeCalculActive.getResultField().getText()));
			
			VZoneCalcul next = getNextZoneDeCalcul();
			if(next != null)
				zoneDeCalculActive = getNextZoneDeCalcul();
		}

		if (zoneDeCalculActive != null)
			zoneDeCalculActive.setEnabled(true);
	}
	
	private VZoneCalcul getNextZoneDeCalcul()
	{
		VZoneCalcul result = null;
		
		for (VZoneCalcul zc : zonesDeCalcul) {
			if (zc == zoneDeCalculActive) {
				int index = zonesDeCalcul.indexOf(zc) - 1;
				if (index == 0)
					isLastZoneCalcul = true;
				if (index < zonesDeCalcul.size())
					result = zonesDeCalcul.get(index);
				else
					result = null;
			}
		}
		
		return result;
	}
	
	public void pushMemento()
	{
		gControl.pushMemento((ArrayList<VPlaque>)plaquesTab.clone());
	}
	
	public void restoreMemento()
	{
		ArrayList<VPlaque> memento = new ArrayList<>(gControl.peekMemento());
		if(memento != null)
		{
			removeAllPlaques(true);
			plaquesTab = memento;
			gControl.popMemento();
			redoPlaquesFromMemento();
			repaintPlaquesPanel();
		}
		else
		{
			System.out.println("No more mementos!");
		}
	}
	
	private void repaintPlaquesPanel()
	{
		plaquesPanel.revalidate();
		plaquesPanel.repaint();
	}
	
	private void redoPlaquesFromMemento()
	{
		for(VPlaque vp : plaquesTab)
			plaquesPanel.add(vp);
	}
	
	private void setupZonesDeCalcul(JPanel zoneCalculPanel) {
		JPanel total = new JPanel(new GridLayout(5, 1));

		for (int cptZC = 0; cptZC < 5; ++cptZC) {
			VZoneCalcul zc = new VZoneCalcul(this);

			VOperande firstOperande = zc.getOperande(Position.FIRST), secondOperande = zc
					.getOperande(Position.SECOND);
			total.add(firstOperande, 0);
			total.add(zc.getSymbole(), 1);
			total.add(secondOperande, 2);
			total.add(zc.getEgal(), 3);
			total.add(zc.getResultField(), 4);
			total.add(zc.getBoutonConfirm(), 5);

			zonesDeCalcul.add(zc);

			zc.setEnabled(false);
		}

		zoneCalculPanel.add(total);
	}

	public GameController getGameController() {
		return gControl;
	}

	public String getProposition() {
		return propositionTA.getText();
	}

	public VZoneCalcul getZoneDeCalculActive() {
		return zoneDeCalculActive;
	}

	private void addPlaquesDeDepart(List<Integer> plaques) {
		for (int cptPlaque = 0; cptPlaque < 6; ++cptPlaque)
			plaquesTab.add(new VPlaque(this));

		for (VPlaque vPlaque : plaquesTab) {
			vPlaque.setText(String.valueOf(gControl.getListePlaques().get(
					plaquesTab.indexOf(vPlaque))));
			plaquesPanel.add(vPlaque);
			vPlaque.addMouseListener(new PlaqueMouseListener(vPlaque));
		}
	}
	
	private void removeAllPlaques(boolean physicalOnly) {
		for (VPlaque vp : plaquesTab)
			plaquesPanel.remove(vp);
		if(!physicalOnly)
			plaquesTab.clear();
		repaint();
	}

	public void addPlaqueFromResultat(int resultat) {
		VPlaque plaque = new VPlaque(this);
		plaque.setText("" + resultat);
		plaquesTab.add(plaque);
		plaquesPanel.add(plaque);
		plaque.addMouseListener(new PlaqueMouseListener(plaque));
		repaint();
	}
	
	public void retablirPlaques()
	{
		retablirPlaque(zoneDeCalculActive.getOperande(Position.FIRST).getText());
		retablirPlaque(zoneDeCalculActive.getOperande(Position.SECOND).getText());
	}
	
	public void retablirPlaque(String valeur) {
		for (VPlaque pl : plaquesTab)
			if (valeur.equals(pl.getText()))
				pl.setEnabled(true);
	}

	public boolean isLastZoneCalcul() {
		return isLastZoneCalcul;
	}

	public void checkEndgameConditions() {
		System.out.println("Check des conditions de victoire!");
		int points = gControl.calcPoints(zoneDeCalculActive.getResultField(),
				propositionTA.getText());
		// Fenetre modale bloquante
		JOptionPane.showMessageDialog(this,
				"La partie est terminee, vous avez gagne " + points
						+ " points!");
		resetGame();
	}

	public void intermediateEndgameDetected() {
		isEndGameDetected = true;
		isLastZoneCalcul = true;
		System.out.println("Fin de partie prematuree detectee!");
		checkEndgameConditions();
	}

	public boolean getIntermediateEndgameDetected() {
		return isEndGameDetected;
	}

	private void resetGame() {
		ArrayList<VPlaque> newPlaques = new ArrayList<>();

		propositionTA.setEnabled(true);
		entrerProp.setEnabled(true);
		propositionTA.setText("");
		removeAllPlaques(false);

		gControl.initBut();
		gControl.initPlaques();

		butPanel.remove(but);
		but = new VBut(this, gControl.getBut());
		butPanel.add(but);
		addPlaquesDeDepart(gControl.getListePlaques());

		for (VZoneCalcul zc : zonesDeCalcul) {
			zc.setEnabled(false);
			zc.clearZoneDeCalcul();
		}
		
		gControl.flushMemento();
		
		zoneDeCalculActive = null;
		resetEndGame();
	}

	public void resetEndGame() {
		isLastZoneCalcul = false;
		isEndGameDetected = false;
	}
}
