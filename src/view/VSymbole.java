package view;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

public class VSymbole extends JLabel {
	private String value = "";

	private VZoneCalcul parentZC;
	private VGame parentGui;

	public VSymbole(VZoneCalcul parent) {
		this.parentZC = parent;
		parentGui = null;

		this.setPreferredSize(new Dimension(60, 60));
		this.setBorder(BorderFactory.createRaisedBevelBorder());
		this.setEnabled(true);
		this.setText(value);
		this.setHorizontalAlignment(CENTER);
		this.setVerticalAlignment(CENTER);
		this.setFont(new Font("sansserif", Font.BOLD, 25));
		this.setVisible(true);
	}

	public VSymbole(VZoneCalcul parent, String valeur) {
		this(parent);

		this.value = valeur;
		this.setPreferredSize(new Dimension(60, 60));
		this.setFont(new Font("sansserif", Font.BOLD, 25));

		this.setEnabled(true);
		this.setText(value);

		this.setVisible(true);
	}

	public VSymbole(VGame parent, String valeur) {
		parentGui = parent;
		parentZC = null;

		this.setPreferredSize(new Dimension(60, 60));
		this.setBorder(BorderFactory.createRaisedBevelBorder());
		this.setEnabled(true);
		this.setText(value);
		this.setHorizontalAlignment(CENTER);
		this.setVerticalAlignment(CENTER);
		this.setFont(new Font("sansserif", Font.BOLD, 25));
		this.setVisible(true);

		this.setEnabled(true);
		this.value = valeur;
		this.setText(value);
	}

	public void setValeur(String valeur) {
		this.value = valeur;
		this.setText(value);
	}

	public String getValeur() {
		return this.getText();
	}

	public VZoneCalcul getZoneCalculParent() {
		return parentZC;
	}

	public VGame getParentGui() {
		return parentGui;
	}

}
