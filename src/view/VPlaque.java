package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import controller.ComponentAnimationListener;
import controller.ComponentSizeAnimation;
import controller.ComponentSizeAnimation.ANIMATIONTYPE;

public class VPlaque extends JButton implements MouseListener {
	/**
     * 
     */
	private static final long serialVersionUID = 1997914082346373368L;
	private ComponentSizeAnimation shrinkNGrowPlaqueAnim;
	private ActionListener shrinkNGrowListener;

	private final int padding = 5;
	private int origX, origY;

	private VGame parentGui;

	public VPlaque(VGame parent) {
		this.parentGui = parent;

		this.setPreferredSize(new Dimension(65, 55));
		this.setMargin(new Insets(0, 0, 0, 0));
		this.setBackground(Color.LIGHT_GRAY);
		this.setForeground(Color.WHITE);
		this.setFont(new Font("sansserif", Font.BOLD, 25));
		this.setVisible(true);
		this.addMouseListener(this);
	}

	@Override
	public void setText(String text) {
		super.setText(text);
		shrinkNGrowPlaqueAnim = new ComponentSizeAnimation(this,
				ANIMATIONTYPE.SHRINKNGROW, new Dimension(1, 1), 100);
		this.removeActionListener(shrinkNGrowListener);
		shrinkNGrowListener = new ComponentAnimationListener(
				new ComponentSizeAnimation[] { shrinkNGrowPlaqueAnim });
		this.addActionListener(shrinkNGrowListener);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		origX = this.getLocation().x;
		origY = this.getLocation().y;
		this.setLocation(this.getLocation().x, this.getLocation().y - padding);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.setLocation(origX, origY);
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	public VGame getParentGui() {
		return parentGui;
	}
}
