package launcher;

import view.MainFrame;
import javax.swing.*;

import org.lwjgl.LWJGLUtil;

import java.io.File;

public class Launcher {
	public static boolean isRunning = true;

	public Launcher() {
		setNativesPath();
	}

	private void setNativesPath() {
		System.out.println("Setting natives @ "
				+ System.getProperty("user.dir"));
		System.out.println("Setting library path @ "
				+ System.getProperty("org.lwjgl.librarypath"));
		System.setProperty("org.lwjgl.librarypath",
				new File(new File(System.getProperty("user.dir"), "native"),
						LWJGLUtil.getPlatformName()).getAbsolutePath());
		System.setProperty("net.java.games.input.librarypath",
				System.getProperty("org.lwjgl.librarypath"));

		System.setProperty("org.lwjgl.util.Debug", "true");
	}

	public static void runGame() {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new MainFrame();
			}
		});
	}

	public static void main(String[] args) {
		Launcher launcher = new Launcher();
		runGame();
	}
}
